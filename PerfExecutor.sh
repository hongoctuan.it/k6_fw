#!/bin/bash
display_help() {
    # taken from https://stackoverflow.com/users/4307337/vincent-stans
    echo "-p Testcase path"
    echo "-s Testcase scenario"
    echo "-e Testcase environment"
    echo "-v Scenario vus"
    echo "-d Scenario duration"
    echo "-t Scenario target"
    echo "-i Scenario iterations"
    echo "-r Scenario rate"
    echo "-x Scenario maxVus"
    echo "-m Scenario maxDuration"
    echo "-t Scenario stages"
    echo "-u Scenario timeUnit"
    echo "-p Scenario preAllocatedVUs"
    echo "-a Scenario startRate"

    exit 1
}
while getopts "p:s:e:v:d:t:i:x:r:m:t:u:p:a" arg; do
  case $arg in
    p) path=$OPTARG;;
    s) scenario=$OPTARG;;
    e) env=$OPTARG;;
    v) vus=$OPTARG;;
    d) duration=$OPTARG;;
    t) target=$OPTARG;;
    i) iterations=$OPTARG;;
    x) maxVus=$OPTARG;;
    r) rate=$OPTARG;;
    m) maxDuration=$OPTARG;;
    t) stages=$OPTARG;;
    u) timeUnit=$OPTARG;;
    p) preVus=$OPTARG;;
    a) startRate=$OPTARG;;
    h) display_help; shift ;;
    *) echo 

  esac
done

K6_INFLUXDB_ORGANIZATION="Dome" \
K6_INFLUXDB_BUCKET="dome" \
K6_INFLUXDB_TOKEN="HEl4j4GyKpECmEh5WfnSaARJsMmdIAn8QKK-mzZwiw84DOfXhB8164sFHnLyt435Qwt-2nHuuWeR3lfgcw_PDQ==" \
K6_INFLUXDB_ADDR="http://localhost:8086" \
K6_BROWSER_ENABLED=true \
K6_BROWSER_HEADLESS=false \
./k6 run $path -e startRate=$startRate -e prevus=$prevus -e timeunit=$timeunit -e scenario=$scenario -e ENV=$env -e vus=$vus -e duration=$duration -e target=$target -e iterations=$iterations -e rate=$rate -e maxVus=$maxVus -e maxDuration=$maxDuration -e stages=$stages -o xk6-influxdb

