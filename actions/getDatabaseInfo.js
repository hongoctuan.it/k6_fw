import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function getDatabaseInfo(cookie, projectId, databaseId) {
  var url = getApiUrl("user/project/"+projectId+entry_point.databaseInfo+databaseId);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "get_database_info"}
  };
  console.log(url)
  let res = http.get(url, _header);
  return res
}