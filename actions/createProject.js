import http from 'k6/http';
import { getApiUrl, getData } from "../common/general.js"
import { uuidv4 } from 'https://jslib.k6.io/k6-utils/1.4.0/index.js';

const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function createProject(cookie) {

  const randomUUID = uuidv4();
  var url = getApiUrl(entry_point.create_project);
  var _data = JSON.stringify({
    "name": data.project_name+randomUUID,
    "organizationId": data.organizationId
  });
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "create_project"},
  };

  let res = http.post(url, _data, params);
  return res
}
