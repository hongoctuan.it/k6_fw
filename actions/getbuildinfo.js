import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function getBuildInfo(cookie) {
  var url = getApiUrl(entry_point.serviceInfo+data.serviceId);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "get_build_info"}
  };
  console.log(url)
  let res = http.get(url, _header);
  return res
}