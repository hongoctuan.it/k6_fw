import http from 'k6/http';
import { getApiUrl, getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function updateOnboardingTasks(cookie) {
  var url = getApiUrl(entry_point.updateOnboardingTasks);
  var _data = JSON.stringify({"deployed-service":true});
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "update_onboarding_task"},
  };

  let res = http.post(url, _data, params);
  return res
}
