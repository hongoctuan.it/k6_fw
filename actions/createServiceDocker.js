import http from 'k6/http';
import { getApiUrl, getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function createServiceDocker(cookie, project_id) {
  var url = getApiUrl("user/project/"+project_id+entry_point.create_service);
  var _data = JSON.stringify({
    "pushDockerImageFlow":true,
    "autoDeployEnabled":true,
    "publicAccessible":true,
    "builder":"paketobuildpacks/builder:full",
    "volumes":[],"replicas":1,
    "verticalAutoscale":
    {
      "scaledRAM":1024,
      "scaledCPU":1000,
      "allocatedRAM":1024,
      "allocatedCPU":1000
    },
    "networking":[
      {"exposed":true,
      "protocol":"HTTP",
      "exposed_port":8080,"port":8080}
    ],
    "isVisible":"public",
    "deployRegion":"US Central",
    "name":data.service_name+Date.now(),
    "environmentVariables":{},
    "buildFromDockerfile":"false"
  });
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "create_service_docker"},
  };
  console.log(url)
  let res = http.post(url, _data, params);
  return res
}
