import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function getHddPrice(cookie) {
  var url = getApiUrl(entry_point.gethddprice);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "get_hdd_price"}
  };
  let _data = JSON.stringify({
    "hdd":2048
  })
  console.log(url)
  let res = http.post(url, _data, _header);
  return res
}