import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function organizationrepos(cookie) {
  var url = getApiUrl("org/"+data.organizationId+entry_point.organizationRepos);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "organization_repos"}
  };
  console.log(url)
  let res = http.get(url, _header);
  return res
}