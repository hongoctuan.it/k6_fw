import http from 'k6/http';
import { getApiUrl, getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function images(cookie,serviceName,serviceId) {
  var url = getApiUrl("user/registry/project/"+serviceName+"-"+serviceId+entry_point.images)
  console.log(url)
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "images"},
  };

  let res = http.get(url, params);
  return res
}
