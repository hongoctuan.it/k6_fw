import http from 'k6/http';
import { getApiUrl, getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function createDB(cookie,projectId, type, username, password, version) {
  var url = getApiUrl("user/project/"+projectId+entry_point.createDB);
  var _data = JSON.stringify({
      "infrastructure":
      {
          "replicas":1,
          "allocatedCPU":250,
          "scaledCPU":250,
          "allocatedMemory":512,
          "scaledMemory":512
      },
      "networkAccess":
      {
          "port":27017,
          "protocol":"TCP"
      },
      "databaseAccess":
      {
          "username":username,
          "password":password
      },
      "visibility":"public",
      "name":"db_"+type+"_"+Date.now(),
      "deployRegion":"US Central",
      "version":version,
      "type":type,
      "diskSize":2048
  });
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "create_mongo_DB"},
  };
  console.log(url)
  let res = http.post(url, _data, params);
  return res
}