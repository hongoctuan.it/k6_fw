import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function organizationrepo(cookie) {
  var url = getApiUrl("org/"+data.organizationId+entry_point.organizationRepo);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "organization_repo"}
  };
  let _data = JSON.stringify({
    "isPrivate":false,
    "url":"https://github.com/jb-dome/dome-demo.git",
    "name":"https-repo-"+data.organizationId+Date.now()
  })
  console.log(url)
  let res = http.post(url, _data, _header);
  return res
}