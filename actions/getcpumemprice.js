import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function getCpuMemPrice(cookie) {
  var url = getApiUrl(entry_point.getcpumemprice);
  let _data = JSON.stringify({
    "allocatedCPU":data.allocatedCPU,
    "allocatedRAM":data.allocatedRAM,
    "scaledCPU":data.scaledCPU,
    "scaledRAM":data.scaledRAM,
    "replicas":data.replicas,
    "serviceID":data.serviceID
  })
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "get_cpu_mem_price"}
  };
  console.log(url)
  let res = http.post(url, _data, _header);
  return res
}
