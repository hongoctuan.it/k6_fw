import { sleep } from 'k6';
import http from 'k6/http';
import { getAppUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function csrf(cookie) {
  var url = getAppUrl(entry_point.crfs);
  let res = http.get(url,{tags: {my_tag: "csrf"}});
  return res
}
