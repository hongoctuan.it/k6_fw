import http from 'k6/http';
import { getAppUrl,getData, getOauth } from "../common/general.js"
const data = getData()
const oauth = getOauth()
const entry_point = JSON.parse(open("../data/entry_point.json"));




export function csrf(cookie) {
  var url = getAppUrl(entry_point.crfs);
  var params = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "csrf"}
  };
  let res = http.get(url, params);
  return res
}



export function login() {
  let crfs_res = csrf(oauth)
  console.log(crfs_res.body)
  let csrfToken = JSON.parse(crfs_res.body).csrfToken
  let csrf_cookie = JSON.parse(JSON.stringify(JSON.parse(JSON.stringify(crfs_res.cookies))['__Host-next-auth.csrf-token'][0])).value
  var url = getAppUrl(entry_point.credentials);
  var params = {
    headers: {
      'cookie':"__Host-next-auth.csrf-token="+csrf_cookie+";"+oauth
    },
    tags: {my_tag: "credentials"}
  };

  var _data = {
    "email": data.username,
    "password": data.password,
    "redirect": "false",
    "callbackUrl": "/",
    "csrfToken": csrfToken,
    "json": "true"
  }
  let res = http.post(url, _data, params);
  return res
}
