import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function serviceRepo(cookie) {
  var url = getApiUrl("service/"+data.serviceId+entry_point.repo);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "service_info"}
  };
  let res = http.get(url, _header);
  return res
}