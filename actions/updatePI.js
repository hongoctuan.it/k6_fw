import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function updatePi(cookie) {
  var url = getApiUrl(entry_point.updatepi);
  var _header = {
    headers: {
      'cookie':cookie
    },
    tags: {my_tag: "update_pi"}
  };
  let _data = {
        "pi":"pi_3NrchdK7tmZGfFmd19jMQbkc_secret_8YGoflnJFLVb56jR2vP27jj2K",
        "futurePayment":false
    }
    console.log(url)
  let res = http.post(url, _data, _header);
  return res
}