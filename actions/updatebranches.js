import { sleep } from 'k6';
import http from 'k6/http';
import { getApiUrl,getData } from "../common/general.js"
const data = getData()
const entry_point = JSON.parse(open("../data/entry_point.json"));

export function updateBranches(cookie,repo) {
  var url = getApiUrl("org/"+data.organizationId+"/repo/"+repo._key+entry_point.updateBranches);
  var _header = {
    headers: {
      "cookie": cookie
    },
    tags: {my_tag: "update_branches"}
  }
  let res = http.patch(url,"", _header)
  return res
}