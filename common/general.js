import { check } from 'k6';
const root_url = JSON.parse(open("../data/entry_point.json"));
const data = JSON.parse(open("../data/data.json"))
const oauth = JSON.parse(open("../data/oauth.json"))

export function setup() {

}

export function getApiUrl(apiPath) {
    let host;
    if (__ENV.ENV === 'production') {
        host = root_url.production.apiurl;
    } else if (__ENV.ENV === 'uat') {
        host = root_url.uat.apiurl;
    } else if (__ENV.ENV === 'dev') {
        host = root_url.dev.apiurl;
    }else if (__ENV.ENV === 'staging') {
        host = root_url.staging.apiurl;
    }
    return `${host}${apiPath}`;
}

export function getAppUrl(apiPath) {
    let host;
    if (__ENV.ENV === 'production') {
        host = root_url.production.appurl;
    } else if (__ENV.ENV === 'uat') {
        host = root_url.uat.appurl;
    } else if (__ENV.ENV === 'dev') {
        host = root_url.dev.appurl;
    }else if (__ENV.ENV === 'staging') {
        host = root_url.staging.appurl;
    }
    return `${host}${apiPath}`;
}

export function getData(){
    let _data;
    if (__ENV.ENV === 'production') {
        _data = data.production;
    } else if (__ENV.ENV === 'uat') {
        _data = data.uat;
    } else if (__ENV.ENV === 'dev') {
        _data = data.dev;
    }else if (__ENV.ENV === 'staging') {
        _data = data.staging;
    }
    return _data;
}

export function getOauth(){
    let _data;
    _data = oauth.name+'='+oauth.value
    return _data;
}

export function verifyReponse(res, type, expected, api_name){
    let passed
    if(type=='status'){
        passed = check(res, {'status verify passed' : (r) => r.status === expected
        },
        { check_tag: api_name });
    }else if(type == "body"){
        passed = check(res, {
            'Check create services': (r) =>
            r.body.includes(expected),
        },
        { check_tag: api_name });
    }
    if (!passed) {
        console.log(`Req failed with Status ${res.status}`)
    }
}