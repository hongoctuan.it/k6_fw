export function getScenario() {
    switch(__ENV.scenario){
        case "shared-iterations":
            return {
                // discardResponseBodies: true,
                scenarios: {
                    contacts: {
                        executor: __ENV.scenario,
                        vus: __ENV.vus,
                        iterations: __ENV.iterations,
                        maxDuration: __ENV.maxDuration,
                },
            },
        };
        case "per-vu-iterations":
            return {
                // discardResponseBodies: true,
                    scenarios: {
                    contacts: {
                        executor: __ENV.scenario,
                        vus: __ENV.vus,
                        iterations: __ENV.iterations,
                        maxDuration: __ENV.maxDuration,
                },
            },
        };
        case "constant-vus":
            return {
                // discardResponseBodies: true,
                scenarios: {
                  contacts: {
                    executor: __ENV.scenario,
                    vus: __ENV.vus,
                    duration: __ENV.duration,
                },
            },
        };
        case "ramping-vus":
            return {
                // discardResponseBodies: true,
                scenarios: {
                  contacts: {
                    executor: __ENV.scenario,
                    startVUs: 0,
                    stages: __ENV.stages,
                    gracefulRampDown: '0s',
                },
            },
        };
        case "constant-arrival-rate":
            return {
                // discardResponseBodies: true,
                scenarios: {
                    contacts: {
                        executor: __ENV.scenario,
                        duration: __ENV.duration,
                        rate: __ENV.rate,
                        timeUnit: __ENV.timeUnit,
                        preAllocatedVUs: __ENV.preVus,
                },
            },
        };
        case "ramping-arrival-rate":
            return {
                // discardResponseBodies: true,
              
                scenarios: {
                    contacts: {
                        executor: __ENV.scenario,
                        startRate: 300,
                        timeUnit: __ENV.timeUnit,
                        preAllocatedVUs: __ENV.preVus,
                        stages: __ENV.stages
                },
            },
        };
        case "ramping-arrival-rate":
            return {
                // discardResponseBodies: true,
                scenarios: {
                    contacts: {
                        executor: __ENV.scenario,
                        startRate: __ENV.startRate,
                        timeUnit: __ENV.timeUnit,
                        preAllocatedVUs: __ENV.preVus,
                        stages: __ENV.stages
                },
            },
        };             
    }
}
