import { verifyReponse, getData } from "../../common/general.js"
import { createServiceDocker } from "../../actions/createServiceDocker.js";
const constant = JSON.parse(open("../../data/constant.json"));
const data = getData()

export default function(){
  let res = createServiceDocker(data.cookie, data.projectId);
  verifyReponse(res,"status",constant.status.success, "createServiceDocker_api")
}