import { verifyReponse, getData } from "../../common/general.js"
const constant = JSON.parse(open("../../data/constant.json"));
import { getScenario } from '../../common/scenario.js';
import { updateOnboardingTasks } from "../../actions/updateOnboardingTasks.js";

const data = getData()
export const options = getScenario()
export default function(){
    console.log(data.cookie)
  let res = updateOnboardingTasks(data.cookie);
  verifyReponse(res,"status",constant.status.success, "updateOnboardingTasks_api")
}