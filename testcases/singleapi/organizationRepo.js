import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js'
import { getData } from "../../common/general.js"
import { organizationrepo } from "../../actions/organizationrepo.js"
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = organizationrepo(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "organizationRepo_api")
}