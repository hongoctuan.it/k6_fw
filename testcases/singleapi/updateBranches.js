import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js'
import { getData } from "../../common/general.js"
import { updateBranches } from "../../actions/updatebranches.js"
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = updateBranches(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "updatebranches_api")
}