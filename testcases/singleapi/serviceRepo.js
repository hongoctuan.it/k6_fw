import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js'
import { getData } from "../../common/general.js"
import { serviceRepo } from "../../actions/servicerepo.js"
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = serviceRepo(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "serviceRepo_api")
}