import { sleep } from 'k6';
import { csrf } from "../../actions/csrf.js"
import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
import { getData } from "../../common/general.js"

const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = csrf(data.oauth2)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "csrf_api")
}