import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
import { getData } from "../../common/general.js"
import { getCpuMemPrice } from '../../actions/getcpumemprice.js';
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = getCpuMemPrice(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "getcpumemprice_api")
}