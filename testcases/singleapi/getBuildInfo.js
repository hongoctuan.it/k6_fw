import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
import { getData } from "../../common/general.js"
import { getBuildInfo } from "../../actions/getbuildinfo.js";
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = getBuildInfo(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "getbuildinfo_api")
}