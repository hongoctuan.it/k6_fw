import {images} from "../../actions/images.js"
import { verifyReponse, getData } from "../../common/general.js"
const constant = JSON.parse(open("../../data/constant.json"));
import { getScenario } from '../../common/scenario.js';
const data = getData()
export const options = getScenario()
export default function(){
  let res = images(data.cookie,data.service_name, data.serviceId);
  console.log(res.body)
  verifyReponse(res,"status",constant.status.create_project_success, "images_api")
}