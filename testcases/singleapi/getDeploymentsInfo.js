import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
import { getData } from "../../common/general.js"
import { getDeploymentsInfo } from "../../actions/getdeploymentsinfo.js";
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = getDeploymentsInfo(data.cookie)
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success, "getDeploymentsInfo_api")
}