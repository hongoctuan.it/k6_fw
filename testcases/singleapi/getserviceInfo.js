import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
import { getData } from "../../common/general.js"
import { getServiceInfo } from "../../actions/getserviceinfo.js";
const data = getData()
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = getServiceInfo(data.cookie)
    verifyReponse(res,"status",constant.status.success, "getServiceInfo_api")
}