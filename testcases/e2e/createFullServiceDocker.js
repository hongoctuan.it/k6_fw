import {createProject} from "../../actions/createProject.js"
import { createServiceDocker } from "../../actions/createServiceDocker.js";
import { getScenario } from '../../common/scenario.js';
import { login } from "../../actions/login.js";
import { getCpuMemPrice } from "../../actions/getcpumemprice.js";
import { getPaymentMethods } from "../../actions/getpaymentmethods.js";
import { serviceRepo } from "../../actions/servicerepo.js";
import { updatePi } from "../../actions/updatePI.js";
import { getServiceInfo } from "../../actions/getserviceinfo.js";
import { getBuildInfo } from "../../actions/getbuildinfo.js";
import { getDeploymentsInfo } from "../../actions/getdeploymentsinfo.js";
import { organizationrepos } from "../../actions/organizationrepos.js";
import { organizationrepo } from "../../actions/organizationrepo.js";
import { updateBranches } from "../../actions/updatebranches.js";
import { verifyReponse } from "../../common/general.js"
import { updateOnboardingTasks } from "../../actions/updateOnboardingTasks.js";
import { images } from "../../actions/images.js";

export const options = getScenario()
export default function(){
    let cookies = login().cookies
    let resCreateProject = createProject(cookies);
    let projectId = JSON.parse(resCreateProject.body).project.id
    getCpuMemPrice(cookies)
    getPaymentMethods(cookies)
    organizationrepos(cookies)
    let resOrg = organizationrepo(cookies)
    let resRepo = JSON.parse(resOrg.body).meta
    let branch = updateBranches(cookies,resRepo)
    let branchHash = JSON.parse(branch.body).repo.Branches[0].hash
    let res = createServiceDocker(cookies, projectId, branchHash, resRepo._key);
    updateOnboardingTasks()
    updatePi(cookies)
    let repo = serviceRepo(cookies)
    console.log()
    images(cookies, repo.body)
    getServiceInfo(cookies)
    getBuildInfo(cookies)
    getDeploymentsInfo(cookies)
    verifyReponse(res,"body","pending payment", "createFullServiceGit")
}