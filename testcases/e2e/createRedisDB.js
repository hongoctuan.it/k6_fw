import {createProject} from "../../actions/createProject.js"
import { getScenario } from '../../common/scenario.js';
import { login } from "../../actions/login.js";
import { getCpuMemPrice } from "../../actions/getcpumemprice.js";
import { getPaymentMethods } from "../../actions/getpaymentmethods.js";
import { getHddPrice } from "../../actions/getHddPrice.js";
import { getDatabaseInfo } from "../../actions/getDatabaseInfo.js";
import { verifyReponse } from "../../common/general.js"
import { createDB } from "../../actions/createDB.js";
import { getData } from "../../common/general.js"
const data = getData()
export const options = getScenario()
export default function(){
    let cookies = login().cookies
    let resCreateProject = createProject(cookies);
    let projectId = JSON.parse(resCreateProject.body).project.id
    getPaymentMethods(cookies)
    getCpuMemPrice(cookies)
    getHddPrice(cookies)
    console.log( data.redis)
    let databases = createDB(cookies, projectId, data.redis.type, data.redis.username, data.redis.password, data.redis.version)
    let databaseId = JSON.parse(databases.body).customerDatabase.id
    let res = getDatabaseInfo(cookies, projectId, databaseId)
    verifyReponse(res,"body","pending payment", "createRedisDB")
}