import {createProject} from "../../actions/createProject.js"
import { getScenario } from '../../common/scenario.js';
import { login } from "../../actions/login.js";
import { verifyReponse } from "../../common/general.js"
const data = JSON.parse(open("../../data/data.json"));
const constant = JSON.parse(open("../../data/constant.json"));
export const options = getScenario()
export default function(){
    let res_login = login()
    let res = createProject(res_login.cookie);
    verifyReponse(res,"status",constant.status.create_project_success, "createProject")
}