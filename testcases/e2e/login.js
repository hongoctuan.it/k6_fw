import { sleep } from 'k6';
import { login } from "../../actions/login.js"
import { verifyReponse } from "../../common/general.js"
import { getScenario } from '../../common/scenario.js';
const constant = JSON.parse(open("../../data/constant.json"));

export const options = getScenario()
export default function(){
    let res = login()
    console.log(res.body)
    verifyReponse(res,"status",constant.status.success)
    sleep(1)
}