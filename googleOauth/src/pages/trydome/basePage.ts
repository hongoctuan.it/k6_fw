import * as fs from 'fs'

export default class BasePage {


    async readFile(path:string){
        const jsonString = fs.readFileSync(path, 'utf-8');
        return jsonString;
    }

    async writeFile(path:string, jsonData:object){
        fs.writeFile('util/google/googlebkcode.json', JSON.stringify(jsonData), (err) => {
            console.log('Error writing file:', err);
        })
    }

    async decodeData(data:string){
        let str = '';
        for (let i = 0; i < data.length; i += 2) {
          const hexValue = data.substr(i, 2);
          const decimalValue = parseInt(hexValue, 16);
          str += String.fromCharCode(decimalValue);
        }
        return str
    }
}