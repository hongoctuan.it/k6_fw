import { Page, Locator, expect } from '@playwright/test'
import BasePage from '../trydome/basePage'
import { fixture } from '../../../hooks/pageFixture'
let basePage: BasePage = new BasePage()
export default class GooglePage {
    readonly page: Page
    readonly email: Locator
    readonly nextButton: Locator
    readonly password: Locator
    readonly passwordNext: Locator
    readonly tryOther: Locator
    readonly backupCodeLink: Locator
    readonly backupCodeInput: Locator
    readonly nextBackupCodeButton: Locator
    readonly emailTryDome: Locator
    readonly passwordTryDome: Locator
    readonly loginTryDomeButton: Locator
    readonly refreshBKCodeButton: Locator
    readonly getNewCodeButton: Locator

    constructor(page: Page) {
        this.page = page
        this.email = this.page.getByLabel('Email or phone')
        this.nextButton = this.page.getByRole('button', { name: 'Next' })
        this.password = this.page.getByLabel('Enter your password')
        this.passwordNext = this.page.getByRole('button', { name: 'Next' })
        this.tryOther = this.page.getByRole('button', {name: 'Try another way'})
        this.backupCodeLink = this.page.locator("//div[@data-challengeid='2']")
        this.backupCodeInput = this.page.getByLabel('Enter a backup code')
        this.nextBackupCodeButton = this.page.getByRole('button', { name: 'Next' })
        this.emailTryDome = this.page.getByLabel("Email Address")
        this.passwordTryDome = this.page.getByLabel("Password")
        this.loginTryDomeButton = this.page.getByRole("button",{ name: 'Log in' })
        this.refreshBKCodeButton = this.page.getByLabel('Generate new codes')
        this.getNewCodeButton = this.page.getByRole('button', {name: 'Get new codes'})    
    }
    async inputEmail() {
        await this.email.fill(process.env.emailUsername || "")
        await this.page.screenshot({ path: 'screenshot.png' });
        await this.nextButton.click()
        console.log("-----input email-----")
    }

    async inputPasswork(){
        await this.page.screenshot({ path: 'screenshot2.png' });
        await new Promise(f => setTimeout(f,3000));  
        await this.password.fill(process.env.emailPassword || "")
        await this.passwordNext.click()
        console.log("-----input password-----")
    }

    async tryOtherWay(code:string){
        const result = this.page.locator("//button[@id='assistiveActionOutOfQuota']");
        if(await result.isVisible()==false) {
            await this.tryOther.click()
            await new Promise(f => setTimeout(f,3000));        
            console.log("-----try orher way-----")
        }
        await this.backupCodeLink.click()
        console.log("-----click login by backup code-----")
        await new Promise(f => setTimeout(f, 3000));
        await this.backupCodeInput.fill(code)
        console.log("-----input backup code-----")
        await new Promise(f => setTimeout(f, 3000));
        await this.nextBackupCodeButton.click()
        console.log("-----login success-----")
    }

    async refreshBKCode(){
        console.log("-----start refress backup code-----")
        let data = await basePage.readFile('util/google/googlebkcode.json')+""
        const jsonData = JSON.parse(data)
        this.refreshBKCodeButton.click()
        await new Promise(f => setTimeout(f, 2000));
        await this.getNewCodeButton.click()
        await new Promise(f => setTimeout(f, 5000));
        const codes = await this.page.$$("//div[@dir='ltr']")
        let count = 1
        for await( const code of codes ){
            let temp = await code.textContent()
            if(temp != '---- ----'){
                switch(count) { 
                    case 1: { 
                        jsonData.code_01 = temp
                        break; 
                    } 
                    case 2: { 
                        jsonData.code_02 = temp
                        break; 
                    } 
                    case 3: { 
                        jsonData.code_03 = temp
                        break; 
                    } 
                    case 4: { 
                        jsonData.code_04 = temp
                        break; 
                    } 
                    case 5: { 
                        jsonData.code_05 = temp
                        break; 
                    } 
                    case 6: { 
                        jsonData.code_06 = temp
                        break; 
                    } 
                    case 7: { 
                        jsonData.code_07 = temp
                        break; 
                    } 
                    case 8: { 
                        jsonData.code_08 = temp
                        break; 
                    } 
                    case 9: { 
                        jsonData.code_09 = temp
                        break; 
                    } 
                    case 10: { 
                        jsonData.code_10 = temp
                        break; 
                    } 
                }

            }else{
                switch(count) { 
                    case 1: { 
                        jsonData.code_01 = 0
                        break; 
                    } 
                    case 2: { 
                        jsonData.code_02 = 0
                        break; 
                    } 
                    case 3: { 
                        jsonData.code_03 = 0
                        break; 
                    } 
                    case 4: { 
                        jsonData.code_04 = 0
                        break; 
                    } 
                    case 5: { 
                        jsonData.code_05 = 0
                        break; 
                    } 
                    case 6: { 
                        jsonData.code_06 = 0
                        break; 
                    } 
                    case 7: { 
                        jsonData.code_07 = 0
                        break; 
                    } 
                    case 8: { 
                        jsonData.code_08 = 0
                        break; 
                    } 
                    case 9: { 
                        jsonData.code_09 = 0
                        break; 
                    } 
                    case 10: { 
                        jsonData.code_10 = 0
                        break; 
                    } 
                }
            }
            count++
        }
        basePage.writeFile('util/google/googlebkcode.json', jsonData)
        console.log("-----done refress backup code-----")
        await new Promise(f => setTimeout(f, 1000));
    }
}