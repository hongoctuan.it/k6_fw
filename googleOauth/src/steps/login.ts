import { Given, When, Then } from '@cucumber/cucumber'
import { fixture } from '../../hooks/pageFixture'
import BasePage from '../pages/trydome/basePage';
import * as fs from 'fs'
const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '../../util/env/.env')
});
let basePage: BasePage = new BasePage()
const jsonData = JSON.parse(fs.readFileSync('test-datas/'+process.env.ENV+'/login.json', 'utf-8'));

Given('I want to get google cookie', {timeout: 10000} , async function () {
    await fixture.page.goto(jsonData['url'])
    await new Promise(r => setTimeout(r, 1000));
    let cookies = await fixture.page.context().cookies()
    for  (var item of cookies){
        if(item.name == '_oauth2_proxy_istio_ingressgateway'){
            let authen = { "name":item.name,"value":item.value}
            fs.writeFile('../data/oauth.json', JSON.stringify(authen), (err) => {
                console.log('Error writing file:', err);
            })
        }

    }
});
