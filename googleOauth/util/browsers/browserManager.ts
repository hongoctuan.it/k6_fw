import { LaunchOptions, chromium, firefox, webkit } from "playwright-core"
const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '../../util/env/.env')
});


const options: LaunchOptions = {
    headless: true
}
export const invokeBrowser = () => {
    const browserType = process.env.BROWSER || "firefox"
    switch (browserType) {
        case "chrome":
            return chromium.launch(options);
        case "firefox":
            return firefox.launch(options);
        case "webkit":
            return webkit.launch(options);
        default:
            throw new Error("Please set the proper browser!")
    }
}