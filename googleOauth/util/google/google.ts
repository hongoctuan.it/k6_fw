import GoolePage from "../../src/pages/google/googlePage"
import { fixture } from '../../hooks/pageFixture'
let googlePage: GoolePage
const path = require('path');
require('dotenv').config({
    path: path.join(__dirname, '../../util/env/.env')
});

export default class google {
    async  getGoogleBKCode(code:string) {
        await fixture.page.goto(process.env.BK_CODE_URL || "")
        googlePage = new GoolePage(fixture.page)
        await googlePage.inputEmail()
        await new Promise(f => setTimeout(f, 3000));
        await googlePage.inputPasswork()
        await new Promise(f => setTimeout(f, 4000));
        await googlePage.tryOtherWay(code)
        await new Promise(f => setTimeout(f, 3000));
        await googlePage.refreshBKCode()
    }

    async loginGoogle(code:string) {
        await fixture.page.goto(process.env.WEB_URL || "")
        googlePage = new GoolePage(fixture.page)
        await googlePage.inputEmail()
        await new Promise(f => setTimeout(f, 2000));
        await googlePage.inputPasswork()
        await new Promise(f => setTimeout(f, 2000));
        await googlePage.tryOtherWay(code)
        await new Promise(f => setTimeout(f, 2000));
    }
}