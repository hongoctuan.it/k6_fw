import { After, AfterAll, Before, BeforeAll } from '@cucumber/cucumber'
import { Browser, BrowserContext, Page } from "@playwright/test"
import { fixture } from './pageFixture'
import { invokeBrowser } from "../util/browsers/browserManager"
import Google from "../util/google/google"
import * as fs from 'fs'
import { getEnv } from '../util/env/env'
import BasePage from '../src/pages/trydome/basePage'

let browser: Browser
let context: BrowserContext
let google: Google
let basePage: BasePage

BeforeAll(async () => {
  getEnv()
  basePage = new BasePage()
  process.env.emailPassword = await basePage.decodeData(process.env.emailPassword || "")+""
  browser = await invokeBrowser()
})

Before( {timeout: 6 * 10000}, async () => {
  context = await browser.newContext({ 
    recordVideo: { 
        dir: 'videos/',
        size: { width: 640, height: 480 }
     } 
});
  fixture.page = await context.newPage();
  google = new Google()
  basePage = new BasePage()
  let data = await basePage.readFile('util/google/googlebkcode.json')+""
  const jsonData = JSON.parse(data)
  if(jsonData.code_08 == 0){
    let code = jsonData.code_09
    await google.getGoogleBKCode(code)
  }else{
    if(jsonData['code_01']!=0){
      await google.loginGoogle(jsonData['code_01'])
      jsonData['code_01']=0
    }else if(jsonData['code_02']!=0){
      await google.loginGoogle(jsonData['code_02'])
      jsonData['code_02']=0
    }else if(jsonData['code_03']!=0){
      await google.loginGoogle(jsonData['code_03'])
      jsonData['code_03']=0
    }else if(jsonData['code_04']!=0){
      await google.loginGoogle(jsonData['code_04'])
      jsonData['code_04']=0
    }else if(jsonData['code_05']!=0){
      await google.loginGoogle(jsonData['code_05'])
      jsonData['code_05']=0
    }else if(jsonData['code_06']!=0){
      await google.loginGoogle(jsonData['code_06'])
      jsonData['code_06']=0
    }else if(jsonData['code_07']!=0){
      await google.loginGoogle(jsonData['code_07'])
      jsonData['code_07']=0
    }else if(jsonData['code_08']!=0){
      await google.loginGoogle(jsonData['code_08'])
      jsonData['code_08']=0
    }
    basePage.writeFile('util/google/googlebkcode.json',jsonData)
  }

})

AfterAll(async () => {
  await fixture.page.close()
});

AfterAll(async () => {
  await browser.close()
})